<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180919142412 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, created_by INT DEFAULT NULL, created_at DATETIME NOT NULL, file_name VARCHAR(255) NOT NULL, actual_file_name VARCHAR(255) NOT NULL, INDEX IDX_8C9F3610DE12AB56 (created_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE storage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_auth (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, auth_hash VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_825FFC90A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firm (id INT AUTO_INCREMENT NOT NULL, created_by INT DEFAULT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, status VARCHAR(100) NOT NULL, INDEX IDX_560581FDDE12AB56 (created_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firm_to_category (firm_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_1E84C38C89AF7860 (firm_id), INDEX IDX_1E84C38C12469DE2 (category_id), PRIMARY KEY(firm_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_3781EC10A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_change (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, user_id INT DEFAULT NULL, field VARCHAR(100) NOT NULL, old_value VARCHAR(1000) DEFAULT NULL, new_value VARCHAR(1000) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_222BBF844584665A (product_id), INDEX IDX_222BBF84A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, created_by INT DEFAULT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, status VARCHAR(100) NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), INDEX IDX_64C19C1DE12AB56 (created_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_input_to_category (category_id INT NOT NULL, category_input_id INT NOT NULL, INDEX IDX_D48FC1AA12469DE2 (category_id), INDEX IDX_D48FC1AA81389EAD (category_input_id), PRIMARY KEY(category_id, category_input_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firm_series (id INT AUTO_INCREMENT NOT NULL, firm_id INT DEFAULT NULL, created_by INT DEFAULT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, status VARCHAR(100) NOT NULL, INDEX IDX_35E0A5C489AF7860 (firm_id), INDEX IDX_35E0A5C4DE12AB56 (created_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firm_series_to_category (firm_series_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_15644818839D514D (firm_series_id), INDEX IDX_1564481812469DE2 (category_id), PRIMARY KEY(firm_series_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivered_product (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, delivery_id INT DEFAULT NULL, count INT NOT NULL, purchase_price INT NOT NULL, sale_price INT NOT NULL, INDEX IDX_57953BD54584665A (product_id), INDEX IDX_57953BD512136921 (delivery_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, storage_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, email VARCHAR(100) NOT NULL, login VARCHAR(100) NOT NULL, phone VARCHAR(100) DEFAULT NULL, INDEX IDX_8D93D6495CC5DB90 (storage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_input (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, type VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, storage_id INT DEFAULT NULL, firm_id INT DEFAULT NULL, firm_series_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, status VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, count INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_D34A04AD5CC5DB90 (storage_id), INDEX IDX_D34A04AD89AF7860 (firm_id), INDEX IDX_D34A04AD839D514D (firm_series_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_photos (product_id INT NOT NULL, uploaded_file_id INT NOT NULL, INDEX IDX_6A0AA17D4584665A (product_id), INDEX IDX_6A0AA17D276973A0 (uploaded_file_id), PRIMARY KEY(product_id, uploaded_file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_input_value (id INT AUTO_INCREMENT NOT NULL, category_input_id INT DEFAULT NULL, product_id INT DEFAULT NULL, value JSON NOT NULL, INDEX IDX_1212D2EB81389EAD (category_input_id), INDEX IDX_1212D2EB4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_auth ADD CONSTRAINT FK_825FFC90A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE firm ADD CONSTRAINT FK_560581FDDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE firm_to_category ADD CONSTRAINT FK_1E84C38C89AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE firm_to_category ADD CONSTRAINT FK_1E84C38C12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery ADD CONSTRAINT FK_3781EC10A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_change ADD CONSTRAINT FK_222BBF844584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_change ADD CONSTRAINT FK_222BBF84A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE category_input_to_category ADD CONSTRAINT FK_D48FC1AA12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_input_to_category ADD CONSTRAINT FK_D48FC1AA81389EAD FOREIGN KEY (category_input_id) REFERENCES category_input (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE firm_series ADD CONSTRAINT FK_35E0A5C489AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id)');
        $this->addSql('ALTER TABLE firm_series ADD CONSTRAINT FK_35E0A5C4DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE firm_series_to_category ADD CONSTRAINT FK_15644818839D514D FOREIGN KEY (firm_series_id) REFERENCES firm_series (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE firm_series_to_category ADD CONSTRAINT FK_1564481812469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivered_product ADD CONSTRAINT FK_57953BD54584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE delivered_product ADD CONSTRAINT FK_57953BD512136921 FOREIGN KEY (delivery_id) REFERENCES delivery (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6495CC5DB90 FOREIGN KEY (storage_id) REFERENCES storage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5CC5DB90 FOREIGN KEY (storage_id) REFERENCES storage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD89AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD839D514D FOREIGN KEY (firm_series_id) REFERENCES firm_series (id)');
        $this->addSql('ALTER TABLE product_photos ADD CONSTRAINT FK_6A0AA17D4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_photos ADD CONSTRAINT FK_6A0AA17D276973A0 FOREIGN KEY (uploaded_file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_input_value ADD CONSTRAINT FK_1212D2EB81389EAD FOREIGN KEY (category_input_id) REFERENCES category_input (id)');
        $this->addSql('ALTER TABLE category_input_value ADD CONSTRAINT FK_1212D2EB4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_photos DROP FOREIGN KEY FK_6A0AA17D276973A0');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6495CC5DB90');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5CC5DB90');
        $this->addSql('ALTER TABLE firm_to_category DROP FOREIGN KEY FK_1E84C38C89AF7860');
        $this->addSql('ALTER TABLE firm_series DROP FOREIGN KEY FK_35E0A5C489AF7860');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD89AF7860');
        $this->addSql('ALTER TABLE delivered_product DROP FOREIGN KEY FK_57953BD512136921');
        $this->addSql('ALTER TABLE firm_to_category DROP FOREIGN KEY FK_1E84C38C12469DE2');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE category_input_to_category DROP FOREIGN KEY FK_D48FC1AA12469DE2');
        $this->addSql('ALTER TABLE firm_series_to_category DROP FOREIGN KEY FK_1564481812469DE2');
        $this->addSql('ALTER TABLE firm_series_to_category DROP FOREIGN KEY FK_15644818839D514D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD839D514D');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610DE12AB56');
        $this->addSql('ALTER TABLE user_auth DROP FOREIGN KEY FK_825FFC90A76ED395');
        $this->addSql('ALTER TABLE firm DROP FOREIGN KEY FK_560581FDDE12AB56');
        $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY FK_3781EC10A76ED395');
        $this->addSql('ALTER TABLE product_change DROP FOREIGN KEY FK_222BBF84A76ED395');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1DE12AB56');
        $this->addSql('ALTER TABLE firm_series DROP FOREIGN KEY FK_35E0A5C4DE12AB56');
        $this->addSql('ALTER TABLE category_input_to_category DROP FOREIGN KEY FK_D48FC1AA81389EAD');
        $this->addSql('ALTER TABLE category_input_value DROP FOREIGN KEY FK_1212D2EB81389EAD');
        $this->addSql('ALTER TABLE product_change DROP FOREIGN KEY FK_222BBF844584665A');
        $this->addSql('ALTER TABLE delivered_product DROP FOREIGN KEY FK_57953BD54584665A');
        $this->addSql('ALTER TABLE product_photos DROP FOREIGN KEY FK_6A0AA17D4584665A');
        $this->addSql('ALTER TABLE category_input_value DROP FOREIGN KEY FK_1212D2EB4584665A');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE storage');
        $this->addSql('DROP TABLE user_auth');
        $this->addSql('DROP TABLE firm');
        $this->addSql('DROP TABLE firm_to_category');
        $this->addSql('DROP TABLE delivery');
        $this->addSql('DROP TABLE product_change');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_input_to_category');
        $this->addSql('DROP TABLE firm_series');
        $this->addSql('DROP TABLE firm_series_to_category');
        $this->addSql('DROP TABLE delivered_product');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE category_input');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_photos');
        $this->addSql('DROP TABLE category_input_value');
    }
}
