<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181003131147 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exchanged_product ADD delivered_product_id INT DEFAULT NULL, ADD sale_price DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE exchanged_product ADD CONSTRAINT FK_919EC2BF51D4BA4F FOREIGN KEY (delivered_product_id) REFERENCES delivered_product (id)');
        $this->addSql('CREATE INDEX IDX_919EC2BF51D4BA4F ON exchanged_product (delivered_product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exchanged_product DROP FOREIGN KEY FK_919EC2BF51D4BA4F');
        $this->addSql('DROP INDEX IDX_919EC2BF51D4BA4F ON exchanged_product');
        $this->addSql('ALTER TABLE exchanged_product DROP delivered_product_id, DROP sale_price');
    }
}
