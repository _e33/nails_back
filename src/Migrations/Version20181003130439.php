<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181003130439 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_return ADD exchange_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_return ADD CONSTRAINT FK_C5E27EAB68AFD1A0 FOREIGN KEY (exchange_id) REFERENCES exchange (id)');
        $this->addSql('CREATE INDEX IDX_C5E27EAB68AFD1A0 ON product_return (exchange_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_return DROP FOREIGN KEY FK_C5E27EAB68AFD1A0');
        $this->addSql('DROP INDEX IDX_C5E27EAB68AFD1A0 ON product_return');
        $this->addSql('ALTER TABLE product_return DROP exchange_id');
    }
}
