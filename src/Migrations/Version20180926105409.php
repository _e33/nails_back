<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180926105409 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sale (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_E54BC005A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_sale (id INT AUTO_INCREMENT NOT NULL, delivered_product_id INT DEFAULT NULL, sale_id INT DEFAULT NULL, count INT NOT NULL, sale_price DOUBLE PRECISION DEFAULT \'0\', INDEX IDX_68A3E2A451D4BA4F (delivered_product_id), INDEX IDX_68A3E2A44A7E4868 (sale_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sale ADD CONSTRAINT FK_E54BC005A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_sale ADD CONSTRAINT FK_68A3E2A451D4BA4F FOREIGN KEY (delivered_product_id) REFERENCES delivered_product (id)');
        $this->addSql('ALTER TABLE product_sale ADD CONSTRAINT FK_68A3E2A44A7E4868 FOREIGN KEY (sale_id) REFERENCES sale (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_sale DROP FOREIGN KEY FK_68A3E2A44A7E4868');
        $this->addSql('DROP TABLE sale');
        $this->addSql('DROP TABLE product_sale');
    }
}
