<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181001120108 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE refund (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, cause LONGTEXT DEFAULT NULL, INDEX IDX_5B2C1458A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_return (id INT AUTO_INCREMENT NOT NULL, sold_product_id INT DEFAULT NULL, refund_id INT DEFAULT NULL, count INT NOT NULL, INDEX IDX_C5E27EABE8EE9BF1 (sold_product_id), INDEX IDX_C5E27EAB189801D5 (refund_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE refund ADD CONSTRAINT FK_5B2C1458A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_return ADD CONSTRAINT FK_C5E27EABE8EE9BF1 FOREIGN KEY (sold_product_id) REFERENCES product_sale (id)');
        $this->addSql('ALTER TABLE product_return ADD CONSTRAINT FK_C5E27EAB189801D5 FOREIGN KEY (refund_id) REFERENCES refund (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_return DROP FOREIGN KEY FK_C5E27EAB189801D5');
        $this->addSql('DROP TABLE refund');
        $this->addSql('DROP TABLE product_return');
    }
}
