<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181003131351 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exchange DROP FOREIGN KEY FK_D33BB079A76ED395');
        $this->addSql('DROP INDEX IDX_D33BB079A76ED395 ON exchange');
        $this->addSql('ALTER TABLE exchange CHANGE user_id created_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB079DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_D33BB079DE12AB56 ON exchange (created_by)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exchange DROP FOREIGN KEY FK_D33BB079DE12AB56');
        $this->addSql('DROP INDEX IDX_D33BB079DE12AB56 ON exchange');
        $this->addSql('ALTER TABLE exchange CHANGE created_by user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB079A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_D33BB079A76ED395 ON exchange (user_id)');
    }
}
