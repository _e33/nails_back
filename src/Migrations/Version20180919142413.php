<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180919142413 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO storage (name) VALUES ("Тбилиси")');

        $this->addSql('INSERT INTO user (name,login,email, storage_id) VALUES ("Альбина", "alya","vitalii.svinchyak@gmail.com", 1)');
        $this->addSql('INSERT INTO user (name,login,email, storage_id) VALUES ("Тимур", "timur","vitalii.svinchyak@gmail.com", 1)');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
