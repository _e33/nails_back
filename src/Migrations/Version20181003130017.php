<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181003130017 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exchange (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, sale_id INT DEFAULT NULL, created_at DATETIME NOT NULL, cause LONGTEXT DEFAULT NULL, INDEX IDX_D33BB079A76ED395 (user_id), INDEX IDX_D33BB0794A7E4868 (sale_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exchanged_product (id INT AUTO_INCREMENT NOT NULL, sold_product_id INT DEFAULT NULL, exchange_id INT DEFAULT NULL, count INT NOT NULL, INDEX IDX_919EC2BFE8EE9BF1 (sold_product_id), INDEX IDX_919EC2BF68AFD1A0 (exchange_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB079A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE exchange ADD CONSTRAINT FK_D33BB0794A7E4868 FOREIGN KEY (sale_id) REFERENCES sale (id)');
        $this->addSql('ALTER TABLE exchanged_product ADD CONSTRAINT FK_919EC2BFE8EE9BF1 FOREIGN KEY (sold_product_id) REFERENCES product_sale (id)');
        $this->addSql('ALTER TABLE exchanged_product ADD CONSTRAINT FK_919EC2BF68AFD1A0 FOREIGN KEY (exchange_id) REFERENCES exchange (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exchanged_product DROP FOREIGN KEY FK_919EC2BF68AFD1A0');
        $this->addSql('DROP TABLE exchange');
        $this->addSql('DROP TABLE exchanged_product');
    }
}
