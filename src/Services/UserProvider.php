<?php

namespace App\Services;

use App\Entity\User;
use App\Entity\UserAuth;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class UserProvider
{

    /**
     * @var User
     */
    private $user = null;

    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function authUser(Request $request): User
    {
        if ($this->user != null) {
            return $this->user;
        }

        $authHeader = $request->headers->get('Authorization');
        if (!$authHeader) {
            throw new AuthenticationException('Not authenticated!');
        }

        $repo = $this->doctrine->getRepository(UserAuth::class);
        /** @var UserAuth $userAuth */
        $userAuth = $repo->findOneBy(['authHash' => $authHeader]);

        if ($userAuth) {
            $this->user = $userAuth->getUser();
        } else {
            throw new AuthenticationException('User not found!');
        }

        return $this->user;
    }

    public function getUser(): User
    {
        if ($this->user === null) {
            throw new AuthenticationException('User was not authenticated!');
        }

        return $this->user;
    }
}