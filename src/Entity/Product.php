<?php

namespace App\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_product", "api_product_full", "api_delivery", "api_sale"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @JMS\Groups({"api_product", "api_product_full", "api_delivery", "api_sale"})
     */
    private $vendorCode;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="name.not_blank")
     * @JMS\Groups({"api_product", "api_product_full", "api_delivery", "api_sale"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="status.not_blank")
     * @JMS\Groups({"api_product_full"})
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="UploadedFile")
     * @ORM\JoinTable(name="product_photos")
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $photos;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="count.not_blank")
     * @JMS\Groups({"api_product", "api_product_full", "api_sale"})
     */
    private $count;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Storage", inversedBy="products")
     * @ORM\JoinColumn(name="storage_id", referencedColumnName="id")
     */
    private $storage;

    /**
     * @ORM\ManyToOne(targetEntity="Firm")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     * @JMS\Groups({"api_product", "api_product_full", "api_delivery"})
     */
    private $firm;

    /**
     * @ORM\ManyToOne(targetEntity="FirmSeries")
     * @ORM\JoinColumn(name="firm_series_id", referencedColumnName="id")
     * @JMS\Groups({"api_product", "api_product_full", "api_delivery"})
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity="CategoryInputValue", mappedBy="product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $inputValues = [];

    /**
     * @ORM\OneToMany(targetEntity="ProductChange", mappedBy="product")
     * @JMS\Groups({"api_product_full"})
     */
    private $changes;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $salePrice;

    /**
     * @ORM\OneToMany(targetEntity="DeliveredProduct", mappedBy="product")
     * @JMS\Groups({"api_product_full", "api_product"})
     */
    private $deliveredProducts;

    private $changesToLog = [];

    public $changedUser;

    /**
     * @ORM\PreUpdate
     * @param PreUpdateEventArgs $event
     */
    public function rememberChanges(PreUpdateEventArgs $event): void
    {
        $this->changesToLog = $event->getEntityChangeSet();
    }

    /**
     * @ORM\PostUpdate
     *
     * @param LifecycleEventArgs $event
     *
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     */
    public function logChanges(LifecycleEventArgs $event): void
    {
        $em = $event->getEntityManager();
        $user = $this->changedUser;

        foreach ($this->changesToLog as $column => $change) {
            if ($column === 'photos') {
                continue;
            }

            if ($column === 'inputValues') {
                /** @var CategoryInputValue[] $change */
                $column = $change[0]->getCategoryInput()->getName();
                $oldValue = $change[0]->getOldValue();
                $newValue = $change[0]->getValue();
                if ($oldValue === $newValue) {
                    continue;
                }
            } else {
                $oldValue = $change[0];
                $newValue = $change[1];
            }

            $productChange = new ProductChange();
            $productChange->setField($column);
            $productChange->setProduct($this);
            $productChange->setOldValue($oldValue);
            $productChange->setNewValue($newValue);
            $productChange->setCreatedBy($user);

            $em->persist($productChange);
        }

        $em->flush();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     * @return Product
     */
    public function setCount($count): Product
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return \Datetime|null
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return Product
     */
    public function setCreatedAt(\Datetime $createdAt): Product
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStorage(): Storage
    {
        return $this->storage;
    }

    /**
     * @return mixed
     */
    public function getChanges(): array
    {
        return $this->changes;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Product
     */
    public function setStatus($status): Product
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param mixed $storage
     * @return Product
     */
    public function setStorage(Storage $storage): Product
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos): void
    {
        $this->photos = $photos;
    }

    /**
     * @return mixed
     */
    public function getFirm()
    {
        return $this->firm;
    }

    /**
     * @param mixed $firm
     */
    public function setFirm($firm): void
    {
        $this->firm = $firm;
    }

    /**
     * @return mixed
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param mixed $series
     */
    public function setSeries($series): void
    {
        $this->series = $series;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return CategoryInputValue[]
     */
    public function getInputValues()
    {
        return $this->inputValues;
    }

    /**
     * @param mixed $inputValues
     */
    public function setInputValues($inputValues): void
    {
        $this->inputValues = $inputValues;
    }

    /**
     * @return mixed
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param int $purchasePrice
     */
    public function setPurchasePrice(int $purchasePrice): void
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * @param int $salePrice
     */
    public function setSalePrice(int $salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    /**
     * @return DeliveredProduct[]
     */
    public function getDeliveredProducts()
    {
        return $this->deliveredProducts;
    }

    /**
     * @return mixed
     */
    public function getVendorCode()
    {
        return $this->vendorCode;
    }

    /**
     * @param mixed $vendorCode
     */
    public function setVendorCode($vendorCode): void
    {
        $this->vendorCode = $vendorCode;
    }
}