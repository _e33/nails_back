<?php

namespace App\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="sale")
 * @ORM\HasLifecycleCallbacks
 */
class Sale
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_sale"})
     */
    private $id;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_sale"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="SoldProduct", mappedBy="sale")
     * @JMS\Groups({"api_sale"})
     */
    private $soldProducts;

    /**
     * @ORM\OneToMany(targetEntity="Refund", mappedBy="sale")
     * @JMS\Groups({"api_sale"})
     */
    private $refunds;

    /**
     * @ORM\OneToMany(targetEntity="Exchange", mappedBy="sale")
     * @JMS\Groups({"api_sale"})
     */
    private $exchanges;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     */
    public function setCreatedAt(\Datetime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return SoldProduct[]
     */
    public function getSoldProducts()
    {
        return $this->soldProducts;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return mixed
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * @param mixed $refunds
     */
    public function setRefunds($refunds): void
    {
        $this->refunds = $refunds;
    }

    /**
     * @return mixed
     */
    public function getExchanges()
    {
        return $this->exchanges;
    }
}