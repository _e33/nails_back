<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="delivery")
 * @ORM\HasLifecycleCallbacks
 */
class Delivery
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_delivery"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="DeliveredProduct", mappedBy="delivery")
     * @JMS\Groups({"api_delivery"})
     */
    private $deliveredProducts;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_product_full", "api_delivery"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JMS\Groups({"api_product_full", "api_delivery"})
     */
    private $createdBy;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return Delivery
     */
    public function setCreatedAt(\Datetime $createdAt): Delivery
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Delivery
     */
    public function setCreatedBy(User $createdBy): Delivery
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return DeliveredProduct[]
     */
    public function getDeliveredProducts()
    {
        return $this->deliveredProducts;
    }

    /**
     * @param mixed $deliveredProducts
     */
    public function setDeliveredProducts($deliveredProducts): void
    {
        $this->deliveredProducts = $deliveredProducts;
    }
}