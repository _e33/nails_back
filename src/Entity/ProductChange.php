<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_change")
 * @ORM\HasLifecycleCallbacks
 */
class ProductChange
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_product_full"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="changes")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"api_product_full"})
     */
    private $field;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @JMS\Groups({"api_product_full"})
     */
    private $oldValue;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @JMS\Groups({"api_product_full"})
     */
    private $newValue;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_product_full"})
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JMS\Groups({"api_product_full"})
     */
    protected $createdBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     * @return ProductChange
     */
    public function setProduct($product): ProductChange
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     * @return ProductChange
     */
    public function setField($field): ProductChange
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @param mixed $oldValue
     * @return ProductChange
     */
    public function setOldValue($oldValue): ProductChange
    {
        $this->oldValue = $oldValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @param mixed $newValue
     * @return ProductChange
     */
    public function setNewValue($newValue): ProductChange
    {
        $this->newValue = $newValue;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return ProductChange
     */
    public function setCreatedAt(\Datetime $createdAt): ProductChange
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }

        //TODO: set created By
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return ProductChange
     */
    public function setCreatedBy(User $createdBy): ProductChange
    {
        $this->createdBy = $createdBy;
        return $this;
    }
}