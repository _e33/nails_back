<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="delivered_product")
 * @ORM\HasLifecycleCallbacks
 */
class DeliveredProduct
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_product"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="deliveredProducts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @JMS\Groups({"api_delivery", "api_sale"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Delivery", inversedBy="deliveredProducts")
     * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
     * @JMS\Groups({"api_product_full"})
     */
    private $delivery;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Groups({"api_product_full", "api_delivery"})
     */
    private $count;

    /**
     * @ORM\Column(type="float")
     * @JMS\Groups({"api_product_full", "api_delivery"})
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="float")
     * @JMS\Groups({"api_product_full", "api_delivery", "api_product"})
     */
    private $salePrice;

    /**
     * @ORM\OneToMany(targetEntity="SoldProduct", mappedBy="deliveredProduct")
     */
    private $soldProducts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    public function getBalanceCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param mixed $purchasePrice
     */
    public function setPurchasePrice(int $purchasePrice): void
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * @param mixed $salePrice
     */
    public function setSalePrice(int $salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    /**
     * @return Delivery
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery($delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @return mixed
     */
    public function getSoldProducts()
    {
        return $this->soldProducts;
    }
}