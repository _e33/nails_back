<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="firm")
 * @ORM\HasLifecycleCallbacks
 */
class Firm
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_firm", "api_product", "api_product_full", "api_delivery"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Expose
     * @Assert\NotBlank(message="name.not_blank")
     * @JMS\Groups({"api_firm", "api_product", "api_product_full", "api_delivery"})
     */
    private $name;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_firm"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"api_firm"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="status.not_blank")
     */
    private $status;

    /**
     * @var FirmSeries[]
     * @ORM\OneToMany(targetEntity="FirmSeries", mappedBy="firm")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     * @JMS\Groups({"api_firm"})
     * @JMS\MaxDepth(1)
     */
    private $series;

    /**
     * Many Firms have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="firms")
     * @ORM\JoinTable(name="firm_to_category")
     * @JMS\Groups({"api_firm"})
     * @JMS\MaxDepth(0)
     */
    private $categories;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return Firm
     */
    public function setCreatedAt(\Datetime $createdAt): Firm
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Firm
     */
    public function setCreatedBy(User $createdBy): Firm
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @param mixed $name
     * @return Firm
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    public function isDeleted(): bool
    {
        return $this->status === self::STATUS_DELETED;
    }

    /**
     * @return FirmSeries[]
     */
    public function getSeries()
    {
        return $this->series;
    }

    public function deleteRecursively()
    {
        $this->setStatus(self::STATUS_DELETED);
        foreach ($this->getSeries() as $child) {
            $child->setStatus(self::STATUS_DELETED);
        }
    }

    public function removeDeletedSeries()
    {
        foreach ($this->getSeries() as $i => $child) {
            if ($child->isDeleted()) {
                unset($this->series[$i]);
            }
        }
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories(array $categories)
    {
        $this->categories = $categories;
    }
}