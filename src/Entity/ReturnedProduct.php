<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_return")
 */
class ReturnedProduct
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_sale"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SoldProduct", inversedBy="returnedProducts")
     * @ORM\JoinColumn(name="sold_product_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $soldProduct;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Groups({"api_sale"})
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity="Refund", inversedBy="returnedProducts")
     * @ORM\JoinColumn(name="refund_id", referencedColumnName="id")
     */
    private $refund;

    /**
     * @ORM\ManyToOne(targetEntity="Exchange", inversedBy="exchangedProducts")
     * @ORM\JoinColumn(name="exchange_id", referencedColumnName="id")
     */
    private $exchange;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSoldProduct()
    {
        return $this->soldProduct;
    }

    /**
     * @param mixed $soldProduct
     */
    public function setSoldProduct($soldProduct): void
    {
        $this->soldProduct = $soldProduct;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count): void
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getRefund()
    {
        return $this->refund;
    }

    /**
     * @param mixed $refund
     */
    public function setRefund($refund): void
    {
        $this->refund = $refund;
    }

    /**
     * @return mixed
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @param mixed $exchange
     */
    public function setExchange($exchange): void
    {
        $this->exchange = $exchange;
    }
}