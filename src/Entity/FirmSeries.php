<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="firm_series")
 * @ORM\HasLifecycleCallbacks
 */
class FirmSeries
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_firm", "api_product", "api_product_full", "api_delivery"})
     */
    private $id;

    /**
     * @var Firm
     * @ORM\ManyToOne(targetEntity="Firm", inversedBy="series")
     * @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     */
    private $firm;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Expose
     * @Assert\NotBlank(message="name.not_blank")
     * @JMS\Groups({"api_firm", "api_product", "api_product_full", "api_delivery"})
     */
    private $name;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_firm"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"api_firm"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="status.not_blank")
     */
    private $status;

    /**
     * Many Firms have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="firmSeries")
     * @ORM\JoinTable(name="firm_series_to_category")
     * @JMS\Groups({"api_firm", "api_product", "api_product_full"})
     * @JMS\MaxDepth(0)
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="series")
     */
    private $products;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return FirmSeries
     */
    public function setCreatedAt(\Datetime $createdAt): FirmSeries
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return FirmSeries
     */
    public function setCreatedBy(User $createdBy): FirmSeries
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @param mixed $name
     * @return FirmSeries
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Firm $firm
     * @return FirmSeries
     */
    public function setFirm(Firm $firm): FirmSeries
    {
        $this->firm = $firm;
        return $this;
    }

    /**
     * @return Firm
     */
    public function getFirm(): ?Firm
    {
        return $this->firm;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    public function isDeleted(): bool
    {
        return $this->status === self::STATUS_DELETED;
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }
}