<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_sale")
 */
class SoldProduct
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_sale"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DeliveredProduct", inversedBy="soldProducts")
     * @ORM\JoinColumn(name="delivered_product_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $deliveredProduct;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="count.not_blank")
     * @JMS\Groups({"api_sale"})
     */
    private $count;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     * @JMS\Groups({"api_sale"})
     */
    private $salePrice;

    /**
     * @ORM\ManyToOne(targetEntity="Sale", inversedBy="soldProducts")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id")
     */
    private $sale;

    /**
     * @ORM\OneToMany(targetEntity="ReturnedProduct", mappedBy="soldProduct")
     * @JMS\Groups({"api_sale"})
     */
    private $returnedProducts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DeliveredProduct
     */
    public function getDeliveredProduct()
    {
        return $this->deliveredProduct;
    }

    /**
     * @param mixed $deliveredProduct
     */
    public function setDeliveredProduct(DeliveredProduct $deliveredProduct): void
    {
        $this->deliveredProduct = $deliveredProduct;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count): void
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * @param mixed $salePrice
     */
    public function setSalePrice($salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param mixed $sale
     */
    public function setSale($sale): void
    {
        $this->sale = $sale;
    }

    /**
     * @return mixed
     */
    public function getReturnedProducts()
    {
        return $this->returnedProducts;
    }
}