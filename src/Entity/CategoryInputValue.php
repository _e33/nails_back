<?php

namespace App\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="category_input_value")
 */
class CategoryInputValue
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api", "api_inlined", "api_firm", "api_product", "api_product_full"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryInput")
     * @ORM\JoinColumn(name="category_input_id", referencedColumnName="id")
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $categoryInput;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="products")
     */
    private $product;

    /**
     * @ORM\Column(type="json")
     * @JMS\Groups({"api_product", "api_product_full"})
     */
    private $value;

    private $oldValue;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CategoryInput
     */
    public function getCategoryInput()
    {
        return $this->categoryInput;
    }

    /**
     * @param mixed $categoryInput
     */
    public function setCategoryInput($categoryInput): void
    {
        $this->categoryInput = $categoryInput;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->copyOldValue();
        $this->value = $value;
    }

    public function copyOldValue()
    {
        $this->oldValue = $this->value;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }
}