<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="movement")
 * @ORM\HasLifecycleCallbacks
 */
class Movement
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_movement"})
     */
    private $id;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_movement"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"api_movement"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="status.not_blank")
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     * @JMS\Groups({"api_movement"})
     */
    private $movedProducts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     */
    public function setCreatedAt(\Datetime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMovedProducts()
    {
        return $this->movedProducts;
    }

    /**
     * @param mixed $movedProducts
     */
    public function setMovedProducts($movedProducts): void
    {
        $this->movedProducts = $movedProducts;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}