<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="refund")
 * @ORM\HasLifecycleCallbacks
 */
class Refund
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_sale"})
     */
    private $id;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_sale"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $createdBy;

    /**
     * @var ReturnedProduct[]
     * @ORM\OneToMany(targetEntity="ReturnedProduct", mappedBy="refund")
     * @JMS\Groups({"api_sale"})
     */
    private $returnedProducts;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Groups({"api_sale"})
     */
    private $cause;

    /**
     * @ORM\ManyToOne(targetEntity="Sale", inversedBy="refunds")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id")
     */
    private $sale;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     */
    public function setCreatedAt(\Datetime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return ReturnedProduct[]
     */
    public function getReturnedProducts(): array
    {
        return $this->returnedProducts;
    }

    /**
     * @param ReturnedProduct[] $returnedProducts
     */
    public function setReturnedProducts(array $returnedProducts): void
    {
        $this->returnedProducts = $returnedProducts;
    }

    /**
     * @return string
     */
    public function getCause(): string
    {
        return $this->cause;
    }

    /**
     * @param string $cause
     */
    public function setCause(string $cause): void
    {
        $this->cause = $cause;
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param mixed $sale
     */
    public function setSale(Sale $sale): void
    {
        $this->sale = $sale;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}