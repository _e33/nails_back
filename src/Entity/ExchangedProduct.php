<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="exchanged_product")
 */
class ExchangedProduct
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api_sale"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SoldProduct", inversedBy="returnedProducts")
     * @ORM\JoinColumn(name="sold_product_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $soldProduct;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Groups({"api_sale"})
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity="Exchange", inversedBy="exchangedProducts")
     * @ORM\JoinColumn(name="exchange_id", referencedColumnName="id")
     */
    private $exchange;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     * @JMS\Groups({"api_sale"})
     */
    private $salePrice;

    /**
     * @ORM\ManyToOne(targetEntity="DeliveredProduct", inversedBy="soldProducts")
     * @ORM\JoinColumn(name="delivered_product_id", referencedColumnName="id")
     * @JMS\Groups({"api_sale"})
     */
    private $deliveredProduct;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSoldProduct()
    {
        return $this->soldProduct;
    }

    /**
     * @param mixed $soldProduct
     */
    public function setSoldProduct($soldProduct): void
    {
        $this->soldProduct = $soldProduct;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count): void
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @param mixed $exchange
     */
    public function setExchange($exchange): void
    {
        $this->exchange = $exchange;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * @param mixed $salePrice
     */
    public function setSalePrice($salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    /**
     * @return mixed
     */
    public function getDeliveredProduct()
    {
        return $this->deliveredProduct;
    }

    /**
     * @param mixed $deliveredProduct
     */
    public function setDeliveredProduct($deliveredProduct): void
    {
        $this->deliveredProduct = $deliveredProduct;
    }
}