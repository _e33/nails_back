<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="file")
 * @ORM\HasLifecycleCallbacks
 */
class UploadedFile
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api", "api_product", "api_product_full"})
     */
    private $id;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api_product_full"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @JMS\MaxDepth(0)
     * @JMS\Groups({"api_product_full"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose
     * @Assert\NotBlank(message="file_name.not_blank")
     * @JMS\Groups({"api_product_full"})
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose
     * @Assert\NotBlank(message="actual_file_name.not_blank")
     * @JMS\Groups({"api_product_full"})
     */
    private $actualFileName;

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getActualFileName()
    {
        return $this->actualFileName;
    }

    /**
     * @param mixed $actualFileName
     */
    public function setActualFileName($actualFileName): void
    {
        $this->actualFileName = $actualFileName;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     */
    public function setCreatedAt(\Datetime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}