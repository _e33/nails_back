<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="category_input")
 * @ORM\HasLifecycleCallbacks
 */
class CategoryInput
{

    const TYPE_COLOR = 'color';
    const TYPE_TEXT = 'text';
    const TYPE_MULTIPLE_TEXT = 'multiple_text';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api", "api_firm", "api_product_full"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Expose
     * @Assert\NotBlank(message="name.not_blank")
     * @JMS\Groups({"api", "api_firm", "api_product", "api_product_full"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Expose
     * @Assert\NotBlank(message="type.not_blank")
     * @JMS\Groups({"api", "api_firm", "api_product_full"})
     */
    private $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }
}