<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="category")
 * @ORM\HasLifecycleCallbacks
 */
class Category
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"api", "api_inlined", "api_firm", "api_product", "api_product_full"})
     */
    private $id;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var Category[]
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @JMS\Groups({"api"})
     * @JMS\MaxDepth(1)
     */
    private $children;

    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Expose
     * @Assert\NotBlank(message="name.not_blank")
     * @JMS\Groups({"api", "api_inlined", "api_firm", "api_product", "api_product_full"})
     */
    private $name;

    /**
     * @var \Datetime $created
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"api"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"api"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="status.not_blank")
     */
    private $status;

    /**
     * @var array
     * @ORM\ManyToMany(targetEntity="CategoryInput")
     * @ORM\JoinTable(name="category_input_to_category")
     * @JMS\Groups({"api", "api_firm"})
     */
    private $inputs;

    /**
     * @var int
     * @JMS\Groups({"api"})
     */
    private $nesting;

    /**
     * Many Firms have Many Categories.
     * @ORM\ManyToMany(targetEntity="Firm", mappedBy="categories")
     */
    private $firms;

    /**
     * Many Firms have Many Categories.
     * @ORM\ManyToMany(targetEntity="FirmSeries", mappedBy="categories")
     */
    private $firmSeries;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->createdAt;
    }

    /**
     * @param \Datetime $createdAt
     * @return Category
     */
    public function setCreatedAt(\Datetime $createdAt): Category
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Category
     */
    public function setCreatedBy(User $createdBy): Category
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Category $parent
     * @return Category
     */
    public function setParent(?Category $parent): Category
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Category
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getNesting(): int
    {
        return $this->nesting;
    }

    /**
     * @param int $nesting
     */
    public function setNesting(int $nesting): void
    {
        $this->nesting = $nesting;
    }

    public function detectNesting()
    {
        $category = $this;
        $nesting = 0;

        while ($category->getParent() !== null) {
            $nesting++;
            $category = $category->getParent();
        }

        $this->setNesting($nesting);
    }

    public function deleteRecursively()
    {
        $this->setStatus(self::STATUS_DELETED);

        foreach ($this->getChildren() as $child) {
            $child->deleteRecursively();
        }
    }

    public function removeDeletedChildren()
    {
        foreach ($this->getChildren() as $i => $child) {
            if ($child->isDeleted()) {
                unset($this->children[$i]);
            }
        }
    }

    /**
     * @return Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    public function isDeleted(): bool
    {
        return $this->status === self::STATUS_DELETED;
    }

    /**
     * @return array
     */
    public function getInputs(): array
    {
        return $this->inputs;
    }

    /**
     * @param array $inputs
     */
    public function setInputs(array $inputs): void
    {
        $this->inputs = $inputs;
    }

    /**
     * @return Firm[]
     */
    public function getFirms()
    {
        return $this->firms;
    }

    /**
     * @return FirmSeries[]
     */
    public function getFirmSeries()
    {
        return $this->firmSeries;
    }
}