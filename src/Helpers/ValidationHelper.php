<?php


namespace App\Helpers;


use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationHelper
{
    /**
     * @param ConstraintViolationListInterface $errors
     *
     * @return array
     */
    public static function getErrorsArray(ConstraintViolationListInterface $errors): array
    {
        if (\count($errors) === 0) {
            return [];
        }

        $newErrors = [];

        /** @var ConstraintViolationInterface $error */
        foreach ($errors as $error) {
            $newErrors[$error->getPropertyPath()] = $error->getMessage();
        }

        return $newErrors;
    }
}
