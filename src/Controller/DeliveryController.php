<?php

namespace App\Controller;

use App\Entity\CategoryInput;
use App\Entity\CategoryInputValue;
use App\Entity\DeliveredProduct;
use App\Entity\Delivery;
use App\Entity\Firm;
use App\Entity\FirmSeries;
use App\Entity\Product;
use App\Entity\Storage;
use App\Entity\UploadedFile;
use App\Helpers\ValidationHelper;
use App\Services\UserProvider;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Context\Context;

/**
 * Product controller.
 *
 * @Route(service="delivery_controller")
 */
class DeliveryController extends Controller
{

    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/delivery")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postDeliveryAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);

        $em = $this->getDoctrine()->getManager();
        $delivery = new Delivery();
        $delivery->setCreatedBy($this->userProvider->getUser());
        $this->fillDelivery($delivery, $request);

        $em->persist($delivery);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Put("/delivery/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putDeliveryAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Delivery::class);
        /** @var Delivery $delivery */
        $delivery = $repo->find($id);
        foreach ($delivery->getDeliveredProducts() as $deliveredProduct) {
            $product = $deliveredProduct->getProduct();
            $product->setCount($product->getCount() - $deliveredProduct->getCount());
            $em->remove($deliveredProduct);
            $em->persist($product);
        }

        $this->fillDelivery($delivery, $request);

        $em->persist($delivery);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    private function fillDelivery(Delivery $delivery, Request $request)
    {
        $user = $this->userProvider->getUser();
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $repo = $em->getRepository(Product::class);

        foreach ($data as $productId => $productData) {
            /** @var Product $product */
            $product = $repo->find($productId);

            $product->setCount($product->getCount() + (int)$productData['count']);
            $product->setPurchasePrice((int)$productData['purchase']);
            $product->setSalePrice((int)$productData['sale']);
            $product->changedUser = $user;

            $deliveredProduct = new DeliveredProduct();
            $deliveredProduct->setProduct($product);
            $deliveredProduct->setCount((int)$productData['count']);
            $deliveredProduct->setPurchasePrice((int)$productData['purchase']);
            $deliveredProduct->setSalePrice((int)$productData['sale']);
            $deliveredProduct->setDelivery($delivery);
            $em->persist($deliveredProduct);
            $em->persist($product);
        }
    }

    /**
     * @FOSRest\Delete("/delivery/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteDeliveryAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $id = (int)$id;

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Delivery::class);
        /** @var Delivery $delivery */
        $delivery = $repo->find($id);
        foreach ($delivery->getDeliveredProducts() as $deliveredProduct) {
            $product = $deliveredProduct->getProduct();
            $product->setCount($product->getCount() - $deliveredProduct->getCount());
            $productDeliveries = $product->getDeliveredProducts();
            $previousDelivery = 0;
            $count = 0;

            foreach ($productDeliveries as $productDelivery) {
                $count++;
                if ($productDelivery->getDelivery()->getId() === $id) {
                    break;
                }

                $previousDelivery = $productDelivery;
            }

            if (count($productDeliveries) === $count) {
                $product->setPurchasePrice($previousDelivery->getPurchasePrice());
                $product->setSalePrice($previousDelivery->getSalePrice());
            } elseif (count($productDeliveries) === 1) {
                $product->setPurchasePrice(0);
                $product->setSalePrice(0);
            }

            $product->changedUser = $user;

            $em->remove($deliveredProduct);
            $em->persist($product);
        }

        $em->remove($delivery);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Get("/delivery_inlined")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getProductInlinedAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $deliveryRepo = $em->getRepository(Delivery::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $deliveryRepo->createQueryBuilder('d');

        if ($request->get('date_from')) {
            $queryBuilder->andWhere('d.createdAt >= :created_from')
                ->setParameter('created_from', new \DateTime($request->get('date_from')));
        }
        if ($request->get('date_to')) {
            $date = new \DateTime($request->get('date_to'));
            $date->add(new \DateInterval('PT23H59M'));
            $queryBuilder->andWhere('d.createdAt <= :created_to')
                ->setParameter('created_to', $date);
        }

        /** @var Product $product */
        $items = $queryBuilder->getQuery()->execute();

        $context = new Context();
        $context->addGroup('api_delivery');
        $data = $this->serializerAdapter->serialize($items, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}