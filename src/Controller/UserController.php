<?php

namespace App\Controller;

//use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Entity\User;
use App\Entity\UserAuth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * User controller.
 *
 * @Route("/")
 */
class UserController extends Controller
{

    /**
     * @FOSRest\Options("/")
     * @FOSRest\Options("/{anything}")
     * @FOSRest\Options("/{anything}/{anything2}")
     * @FOSRest\Options("/{anything}/{anything2}/{anything3}")
     */
    public function options()
    {
        return $this->json([], Response::HTTP_OK);
    }

    /**
     * Create Article.
     * @FOSRest\Post("/user")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postUserAction(Request $request)
    {
        $user = new User();
        $user->setName('her');
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Create Client.
     * @FOSRest\Post("/auth")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function authAction(Request $request)
    {
        $username = mb_strtolower($request->get('username'));

        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->findOneBy(['login' => $username]);

        if (!$user) {
            return $this->json(null, Response::HTTP_UNAUTHORIZED);
        }

        $authHash = bin2hex(random_bytes(50));
        $userAuth = new UserAuth();
        $userAuth->setUser($user);
        $userAuth->setAuthHash($authHash);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userAuth);
        $em->flush();

        return $this->json(["auth" => $userAuth->getAuthHash()], Response::HTTP_ACCEPTED);
    }
}