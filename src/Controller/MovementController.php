<?php

namespace App\Controller;

use App\Entity\CategoryInput;
use App\Entity\CategoryInputValue;
use App\Entity\Firm;
use App\Entity\FirmSeries;
use App\Entity\Movement;
use App\Entity\Product;
use App\Entity\Storage;
use App\Entity\UploadedFile;
use App\Helpers\ValidationHelper;
use App\Services\UserProvider;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Context\Context;

/**
 * Product controller.
 *
 * @Route(service="movement_controller")
 */
class MovementController extends Controller
{
    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/movement")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postMovementAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $movement = new Movement();
        $movement->setCreatedBy($user);
        $movement->setMovedProducts(json_encode($request->get('moved_products')));
        $movement->setStatus(Movement::STATUS_ACTIVE);

        $productsRepo = $em->getRepository(Product::class);

        foreach ($request->get('moved_products') as $productId => $count) {
            /** @var Product $product */
            $product = $productsRepo->find($productId);
            $product->changedUser = $user;
            $product->setCount($product->getCount() - $count);
            $em->persist($product);
        }

        $em->persist($movement);
        $em->flush();

        return new JsonResponse([], Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Put("/movement/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putProductAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $movementRepo = $em->getRepository(Movement::class);
        $productRepo = $em->getRepository(Product::class);
        /** @var Movement $movement */
        $movement = $movementRepo->find($id);
        $alreadyMovedProducts = json_decode($movement->getMovedProducts(), true);

        $movedProducts = [];

        foreach ($request->get('moved_products') as $movedProduct) {
            $currentMovedProductCount = $alreadyMovedProducts[$movedProduct['id']] ?? null;

            if ($currentMovedProductCount !== $movedProduct['count']) {
                /** @var Product $product */
                $product = $productRepo->find($movedProduct['id']);
                $product->setCount($product->getCount() + $currentMovedProductCount - $movedProduct['count']);
                $product->changedUser = $user;
                $em->persist($product);

                $movedProducts[$movedProduct['id']] = $movedProduct['count'];
            } else {
                $movedProducts[$movedProduct['id']] = $movedProduct['count'];
            }
        }

        $movement->setMovedProducts(json_encode($movedProducts));
        $em->persist($movement);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Delete("/movement/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteMovementAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $movementRepo = $em->getRepository(Movement::class);
        $productRepo = $em->getRepository(Product::class);
        /** @var Movement $movement */
        $movement = $movementRepo->find($id);
        $products = json_decode($movement->getMovedProducts(), true);

        foreach ($products as $productId => $count) {
            /** @var Product $product */
            $product = $productRepo->find($productId);
            $product->changedUser = $user;
            $product->setCount($product->getCount() + $count);
            $em->persist($product);
        }

        $em->remove($movement);
        $em->flush();

        return $this->json(null, Response::HTTP_OK);
    }

    /**
     * @FOSRest\Get("/movement/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getMovementAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $movementRepo = $em->getRepository(Movement::class);
        $productRepo = $em->getRepository(Product::class);
        /** @var Movement $movement */
        $movement = $movementRepo->find($id);
        $products = [];

        foreach (json_decode($movement->getMovedProducts()) as $productId => $count) {
            $name = $productRepo->find($productId)->getName();
            $products[] = [
                'id' => $productId,
                'count' => $count,
                'name' => $name
            ];
        }

        $movement->setMovedProducts(json_encode($products));

        $context = new Context();
        $context->addGroup('api_movement');
        $data = $this->serializerAdapter->serialize($movement, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/movement_inlined")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getMovementInlinedAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $movementRepo = $em->getRepository(Movement::class);
        /** @var Product $product */
        $movements = $movementRepo->findBy(['status' => Product::STATUS_ACTIVE]);

        $context = new Context();
        $context->addGroup('api_movement');
        $data = $this->serializerAdapter->serialize($movements, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}