<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\CategoryInput;
use App\Helpers\ValidationHelper;
use App\Services\UserProvider;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Context\Context;

/**
 * Category controller.
 *
 * @Route(service="category_controller")
 */
class CategoryController extends Controller
{
    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/category")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postCategoryAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $category = new Category();
        $category->setName($request->get('name'));

        if ($request->get('parent')) {
            $parentCategory = $em->getRepository(Category::class)
                ->findOneBy(['id' => $request->get('parent')]);
            $category->setParent($parentCategory);
        }

        $this->setCategoryInputs($request, $category);
        $category->setCreatedBy($user);
        $category->setStatus(Category::STATUS_ACTIVE);

        $errors = $validator->validate($category);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $em->persist($category);
        $em->flush();

        $context = new Context();
        $context->addGroup('api');
        $data = $this->serializerAdapter->serialize($category, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Put("/category/{id}")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putCategoryAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $this->userProvider->authUser($request);

        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $categoriesRepo = $em->getRepository(Category::class);
        /** @var Category $category */
        $category = $categoriesRepo->find($id);
        $category->setName($request->get('name'));

        if ($request->get('parent')) {
            $parentCategory = $em->getRepository(Category::class)
                ->findOneBy(['id' => $request->get('parent')]);
            $category->setParent($parentCategory);
        } else {
            $category->setParent(null);
        }

        $this->setCategoryInputs($request, $category);
        $category->setStatus(Category::STATUS_ACTIVE);

        $errors = $validator->validate($category);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $em->persist($category);
        $em->flush();

        $context = new Context();
        $context->addGroup('api');
        $data = $this->serializerAdapter->serialize($category, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    private function setCategoryInputs(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$request->get('inputs')) {
            $category->setInputs([]);
        }

        $inputsRepo = $em->getRepository(CategoryInput::class);
        $inputs = [];

        foreach ($request->get('inputs') as $input) {
            $categoryInput = null;
            if (isset($input['id'])) {
                /** @var CategoryInput $categoryInput */
                $categoryInput = $inputsRepo->find($input['id']);
                if ($categoryInput->getName() !== $input['name']) {
                    $categoryInput->setName($input['name']);
                    $em->persist($categoryInput);
                }
            } else {
                $categoryInput = new CategoryInput();
                $categoryInput->setName($input['name']);
                $categoryInput->setType($input['type']);
                $em->persist($categoryInput);
            }

            $inputs[] = $categoryInput;
        }

        $category->setInputs($inputs);

    }

    /**
     * @FOSRest\Get("/category")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getCategoriesAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $categoriesRepo = $em->getRepository(Category::class);
        /** @var Category[] $categories */
        $categories = $categoriesRepo->findBy(['status' => Category::STATUS_ACTIVE]);

        foreach ($categories as $i => $category) {
            $category->detectNesting();
            $category->removeDeletedChildren();

            if ($category->getNesting() !== 0) {
                unset($categories[$i]);
            }
        }

        $context = new Context();
        $context->addGroup('api');
        $data = $this->serializerAdapter->serialize(array_values($categories), 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/category_inlined")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getCategoriesInlinedAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $categoriesRepo = $em->getRepository(Category::class);
        /** @var Category[] $categories */
        $categories = $categoriesRepo->findBy(['status' => Category::STATUS_ACTIVE]);

        $context = new Context();
        $context->addGroup('api_inlined');
        $data = $this->serializerAdapter->serialize(array_values($categories), 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Delete("/category/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteCategoryAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $categoriesRepo = $em->getRepository(Category::class);
        /** @var Category $category */
        $category = $categoriesRepo->find($id);

        if (count($category->getChildren()) > 0) {
            $metadata = [];
            foreach ($category->getChildren() as $childCategory) {
                if ($childCategory->isDeleted()) {
                    continue;
                }
                $metadata[] = $childCategory->getName();
            }

            if (count($metadata) > 0) {
                return $this->json(['reason' => 'existed.children', 'metadata' => $metadata], Response::HTTP_BAD_REQUEST);
            }
        }

        if (count($category->getFirms()) > 0) {
            $metadata = [];
            foreach ($category->getFirms() as $firm) {
                if ($firm->isDeleted()) {
                    continue;
                }
                $metadata[] = $firm->getName();
            }

            if (count($metadata) > 0) {
                return $this->json(['reason' => 'existed.firms', 'metadata' => $metadata], Response::HTTP_BAD_REQUEST);
            }
        }

        if (count($category->getFirmSeries()) > 0) {
            $metadata = [];
            foreach ($category->getFirmSeries() as $series) {
                if ($series->isDeleted()) {
                    continue;
                }
                $firmName = $series->getFirm()->getName();

                if (!isset($metadata[$firmName])) {
                    $metadata[$firmName] = [];
                }

                $metadata[$firmName][] = $series->getName();
            }

            if (count($metadata) > 0) {
                return $this->json(['reason' => 'existed.firms_series', 'metadata' => $metadata], Response::HTTP_BAD_REQUEST);
            }
        }

        $category->deleteRecursively();
        $em->persist($category);

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Get("/category_inputs")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getCategoryInputsAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $inputsRepo = $em->getRepository(CategoryInput::class);
        /** @var Category[] $inputs */
        $inputs = $inputsRepo->findAll();

        $context = new Context();
        $context->addGroup('api');
        $data = $this->serializerAdapter->serialize(array_values($inputs), 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}