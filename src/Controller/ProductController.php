<?php

namespace App\Controller;

use App\Entity\CategoryInput;
use App\Entity\CategoryInputValue;
use App\Entity\Firm;
use App\Entity\FirmSeries;
use App\Entity\Product;
use App\Entity\Storage;
use App\Entity\UploadedFile;
use App\Helpers\ValidationHelper;
use App\Services\UserProvider;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Context\Context;

/**
 * Product controller.
 *
 * @Route(service="product_controller")
 */
class ProductController extends Controller
{
    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/product/photo")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postUploadFileAction(Request $request)
    {
        $user = $this->userProvider->authUser($request);

        $manager = $this->getDoctrine()->getManager();
        $savedFiles = [];

        foreach ($request->files->all() as $file) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $originalName = $file->getClientOriginalName();
            $file->move($this->container->getParameter('file_directory'), $fileName);

            $fileEntity = new UploadedFile ();
            $fileEntity->setFileName($fileName);
            $fileEntity->setActualFileName($originalName);
            $fileEntity->setCreatedBy($user);

            $manager->persist($fileEntity);
            $savedFiles[] = $fileEntity;
        }
        $manager->flush();

        $context = new Context();
        $context->addGroup('api');
        $data = $this->serializerAdapter->serialize($savedFiles, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/product/photo/{id}")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getUploadedFileAction(Request $request)
    {
        $id = $request->get('id');
        $manager = $this->getDoctrine()->getManager();
        /** @var UploadedFile $photo */
        $photo = $manager->getRepository(UploadedFile::class)->find($id);
        $path = $this->container->getParameter('file_directory') . $photo->getFileName();
        $response = $this->file($path);

        return $response;
    }

    /**
     * @FOSRest\Post("/product")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postProductAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $product = $this->fillProduct($request, new Product());
        $product->setStatus(Product::STATUS_ACTIVE);
        $product->setCount(0);

        $errors = $validator->validate($product);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $storageRepo = $em->getRepository(Storage::class);
        $storage = $storageRepo->find(1);

        $product->setStorage($storage);
        $em->persist($product);
        $em->flush();

        $context = new Context();
        $context->addGroup('api_product');
        $data = $this->serializerAdapter->serialize($product, 'json', $context);

        return new JsonResponse($data, Response::HTTP_CREATED, [], true);
    }

    /**
     * @FOSRest\Put("/product/{id}")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putProductAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var Product $product */
        $product = $productRepo->find($id);
        $this->fillProduct($request, $product);
        $product->changedUser = $user;

        $errors = $validator->validate($product);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em->persist($product);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    private function fillProduct(Request $request, Product $product): Product
    {
        $em = $this->getDoctrine()->getManager();
        $product->setName($request->get('name'));
        $product->setDescription($request->get('description'));

        if ($request->get('vendorCode')) {
            $product->setVendorCode($request->get('vendorCode'));
        } elseif ($product->getVendorCode() === null) {
            $product->setVendorCode('nails-' . rand(100, 999) . '-' . rand(100, 999) . '-' . rand(100, 999));
        }

        $oldFirmAndSeriesHash = $product->getFirm() ? $product->getFirm()->getId() . '_' . $product->getSeries()->getId() : '';

        $firm = $em->getRepository(Firm::class)->find($request->get('firm'));
        $product->setFirm($firm);
        $series = $em->getRepository(FirmSeries::class)->find($request->get('firm_series'));
        $product->setSeries($series);

        $newFirmAndSeriesHash = $product->getFirm()->getId() . '_' . $product->getSeries()->getId();

        $firmOrSeriesChanged = $oldFirmAndSeriesHash !== $newFirmAndSeriesHash;

        if ($firmOrSeriesChanged) {
            foreach ($product->getInputValues() as $value) {
                $em->remove($value);
            }
        }

        if ($request->get('photos')) {
            $photos = $em->getRepository(UploadedFile::class)->findBy(['id' => $request->get('photos')]);
            $product->setPhotos($photos);
        }

        if ($request->get('customInputsValues')) {
            $repo = $em->getRepository(CategoryInput::class);
            $inputs = [];
            foreach ($request->get('customInputsValues') as $input) {
                if ($input['value'] === '') {
                    continue;
                }
                /** @var CategoryInputValue $inputValue */
                $inputValue = null;

                if (!$firmOrSeriesChanged && $product->getInputValues() && count($product->getInputValues()) != 0) {
                    foreach ($product->getInputValues() as $inputVal) {
                        if ($inputVal->getCategoryInput()->getId() === $input['id']) {
                            $inputValue = $inputVal;
                            break;
                        }
                    }
                } else {
                    /** @var CategoryInput $inputEntity */
                    $inputEntity = $repo->find($input['id']);
                    $inputValue = new CategoryInputValue();
                    $inputValue->setProduct($product);
                    $inputValue->setCategoryInput($inputEntity);
                }


                $inputValue->setValue($input['value']);
                $em->persist($inputValue);

                $inputs[] = $inputValue;
            }

            $product->setInputValues($inputs);
        }

        return $product;
    }

    /**
     * @FOSRest\Delete("/product/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteProductAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var Product $product */
        $product = $productRepo->find($id);
        $product->changedUser = $user;

        $product->setStatus(Product::STATUS_DELETED);
        $em->persist($product);
        $em->flush();

        return $this->json(null, Response::HTTP_OK);
    }

    /**
     * @FOSRest\Get("/product/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getProductAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var Product $product */
        $product = $productRepo->find($id);
        $context = new Context();
        $context->addGroup('api_product_full');
        $data = $this->serializerAdapter->serialize($product, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/product_inlined")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getProductInlinedAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var Product $product */
        $products = $productRepo->findBy(['status' => Product::STATUS_ACTIVE]);

        $context = new Context();
        $context->addGroup('api_product');
        $data = $this->serializerAdapter->serialize($products, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/product/search/{name}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function searchProductAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $name = $request->get('name');

        if (!$name) {
            return $this->json(['name' => 'name.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var Product $products */
        $products = $productRepo->createQueryBuilder('p')
            ->andWhere('p.name LIKE :name')
            ->setParameter('name', "%{$name}%")
            ->getQuery()
            ->getResult();
        $context = new Context();

        return new JsonResponse($this->serializerAdapter->serialize($products, 'json', $context), Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Post("/product_filtered")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getFilteredProducts(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $productRepo->createQueryBuilder('p')
            ->leftJoin('p.firm', 'f')
            ->leftJoin('p.series', 's');
        $i = 1;
        $firmsFilter = $request->get('firms');
        $existedFilter = $request->get('existed');
        $nameOrVendorCodeFilter = $request->get('nameOrVendorCode');

        if ($firmsFilter) {
            foreach ($firmsFilter as $firmId => $seriesIds) {
                if (count($seriesIds) > 0) {
                    $queryBuilder->orWhere("f.id = :firm_{$i} AND s.id IN (:series_{$i})")
                        ->setParameter("firm_{$i}", $firmId)
                        ->setParameter("series_{$i}", $seriesIds);
                } else {
                    $queryBuilder->orWhere("f.id = :firm_{$i}")
                        ->setParameter("firm_{$i}", $firmId);
                }
                $i++;
            }
        }

        if ($existedFilter) {
            $queryBuilder->andWhere('p.count > 0');
        }

        if ($nameOrVendorCodeFilter) {
            $queryBuilder->andWhere('p.name LIKE :nameOrVendorCodeFilter OR p.vendorCode LIKE :nameOrVendorCodeFilter')
                ->setParameter('nameOrVendorCodeFilter', "%{$nameOrVendorCodeFilter}%");
        }

        $queryBuilder->andWhere('p.status = :status')
            ->setParameter('status', Product::STATUS_ACTIVE);

        $products = $queryBuilder->getQuery()->execute();

        $context = new Context();
        $context->addGroup('api_product');
        $data = $this->serializerAdapter->serialize($products, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}