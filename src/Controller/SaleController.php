<?php

namespace App\Controller;

use App\Entity\Exchange;
use App\Entity\ExchangedProduct;
use App\Entity\Product;
use App\Entity\Refund;
use App\Entity\ReturnedProduct;
use App\Entity\Sale;
use App\Entity\SoldProduct;
use App\Services\UserProvider;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller.
 *
 * @Route(service="sale_controller")
 */
class SaleController extends Controller
{
    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/sale")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postSaleAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $sale = new Sale();
        $sale->setCreatedBy($user);
        $em->persist($sale);

        $this->fillSale($sale, $request);

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Put("/sale/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putProductAction(Request $request): JsonResponse
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $this->userProvider->authUser($request);
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $saleRepo = $em->getRepository(Sale::class);
        /** @var Sale $sale */
        $sale = $saleRepo->find($id);

        foreach ($sale->getSoldProducts() as $soldProduct) {
            $product = $soldProduct->getDeliveredProduct()->getProduct();
            $product->setCount($product->getCount() + $soldProduct->getCount());
            $em->persist($product);
            $em->remove($soldProduct);
        }


        $this->fillSale($sale, $request);
        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    private function fillSale(Sale $sale, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository(Product::class);
        $data = $request->get('sale');
        $user = $this->userProvider->getUser();

        foreach ($data as $productToSale) {
            /** @var Product $product */
            $product = $productRepo->find($productToSale['id']);
            $deliveredProductToSail = null;

            foreach ($product->getDeliveredProducts() as $deliveredProduct) {
                if ($deliveredProduct->getBalanceCount() > 0) {
                    $deliveredProductToSail = $deliveredProduct;
                    break;
                }
            }

            $soldProduct = new SoldProduct();
            $soldProduct->setCount((int)$productToSale['count']);
            $soldProduct->setSalePrice((float)$productToSale['sale_price']);
            $soldProduct->setDeliveredProduct($deliveredProductToSail);
            $soldProduct->setSale($sale);

            if ($product->getCount() < $soldProduct->getCount()) {
                throw new \LogicException('Не хватает продуктов на складе!' . "{$product->getId()} / {$product->getCount()} - {$soldProduct->getCount()}");
            }

            $product->setCount($product->getCount() - $soldProduct->getCount());
            $product->changedUser = $user;

            $em->persist($product);
            $em->persist($soldProduct);
        }
    }

    /**
     * @FOSRest\Get("/sale")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getSalesAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $saleRepo = $em->getRepository(Sale::class);
        /** @var Product $product */
        $sales = $saleRepo->findAll();

        $context = new Context();
        $context->addGroup('api_sale');
        $data = $this->serializerAdapter->serialize($sales, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/sale/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getSaleAction(Request $request): JsonResponse
    {
        $id = $request->get('id');
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $saleRepo = $em->getRepository(Sale::class);
        /** @var Product $product */
        $sale = $saleRepo->find($id);

        $context = new Context();
        $context->addGroup('api_sale');
        $data = $this->serializerAdapter->serialize($sale, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Post("/sale/refund/{saleId}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postRefundAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);

        $em = $this->getDoctrine()->getManager();
        $saleRepo = $em->getRepository(Sale::class);
        $soldProductsRepo = $em->getRepository(SoldProduct::class);
        $id = $request->get('saleId');
        $cause = $request->get('cause');
        $returnedProducts = $request->get('returnedProducts');

        $sale = $saleRepo->find($id);
        $refund = new Refund();
        $refund->setCreatedBy($user);
        $refund->setCause($cause);
        $refund->setSale($sale);
        $em->persist($refund);

        foreach ($returnedProducts as $soldProductId => $count) {
            /** @var SoldProduct $soldProduct */
            $soldProduct = $soldProductsRepo->find($soldProductId);
            $product = $soldProduct->getDeliveredProduct()->getProduct();

            $returnedProduct = new ReturnedProduct();
            $returnedProduct->setSoldProduct($soldProduct);
            $returnedProduct->setCount($count);
            $returnedProduct->setRefund($refund);

            $product->setCount($product->getCount() + $count);
            $product->changedUser = $user;

            $em->persist($returnedProduct);
            $em->persist($product);
        }

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Post("/sale/exchange/{saleId}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postExchangeAction(Request $request): JsonResponse
    {
        $user = $this->userProvider->authUser($request);

        $em = $this->getDoctrine()->getManager();
        $saleRepo = $em->getRepository(Sale::class);
        $soldProductsRepo = $em->getRepository(SoldProduct::class);
        $productRepo = $em->getRepository(Product::class);

        $id = $request->get('saleId');
        $cause = $request->get('cause');
        $returnedProducts = $request->get('returnedProducts');
        $exchangedProducts = $request->get('exchangedProducts');

        $sale = $saleRepo->find($id);
        $exchange = new Exchange();
        $exchange->setCreatedBy($user);
        $exchange->setSale($sale);
        $exchange->setCause($cause);
        $em->persist($exchange);

        foreach ($returnedProducts as $soldProductId => $count) {
            /** @var SoldProduct $exchangedProduct */
            $exchangedProduct = $soldProductsRepo->find($soldProductId);
            $product = $exchangedProduct->getDeliveredProduct()->getProduct();

            $returnedProduct = new ReturnedProduct();
            $returnedProduct->setSoldProduct($exchangedProduct);
            $returnedProduct->setCount($count);
            $returnedProduct->setExchange($exchange);

            $product->setCount($product->getCount() + $count);
            $product->changedUser = $user;

            $em->persist($returnedProduct);
            $em->persist($product);
        }

        foreach ($exchangedProducts as $productToSale) {
            /** @var Product $product */
            $product = $productRepo->find($productToSale['id']);
            $deliveredProductToSail = null;

            foreach ($product->getDeliveredProducts() as $deliveredProduct) {
                if ($deliveredProduct->getBalanceCount() > 0) {
                    $deliveredProductToSail = $deliveredProduct;
                    break;
                }
            }

            $exchangedProduct = new ExchangedProduct();
            $exchangedProduct->setCount((int)$productToSale['count']);
            $exchangedProduct->setSalePrice((float)$productToSale['sale_price']);
            $exchangedProduct->setDeliveredProduct($deliveredProductToSail);
            $exchangedProduct->setExchange($exchange);

            if ($product->getCount() < $exchangedProduct->getCount()) {
                throw new \LogicException('Не хватает продуктов на складе!' . "{$product->getId()} / {$product->getCount()} - {$exchangedProduct->getCount()}");
            }

            $product->setCount($product->getCount() - $exchangedProduct->getCount());
            $product->changedUser = $user;

            $em->persist($product);
            $em->persist($exchangedProduct);
        }

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }
}