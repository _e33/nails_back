<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Firm;
use App\Entity\FirmSeries;
use App\Helpers\ValidationHelper;
use App\Services\UserProvider;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Serializer\JMSSerializerAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use FOS\RestBundle\Context\Context;

/**
 * Firm controller.
 *
 * @Route(service="firm_controller")
 */
class FirmController extends Controller
{
    private $serializerAdapter;
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @param JMSSerializerAdapter $serializerAdapter
     * @param UserProvider $userProvider
     */
    public function setJMSSerializerAdapter(JMSSerializerAdapter $serializerAdapter, UserProvider $userProvider)
    {
        $this->serializerAdapter = $serializerAdapter;
        $this->userProvider = $userProvider;
    }

    /**
     * @FOSRest\Post("/firm")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postFirmAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $firm = new Firm();
        $firm->setName($request->get('name'));

        $firm->setCreatedBy($user);
        $firm->setStatus(Firm::STATUS_ACTIVE);

        if ($request->get('categories')) {
            $categories = $em->getRepository(Category::class)->findBy(['id' => $request->get('categories')]);
            $firm->setCategories($categories);
        }

        $errors = $validator->validate($firm);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $em->persist($firm);
        $em->flush();

        $context = new Context();
        $context->addGroup('api_firm');
        $data = $this->serializerAdapter->serialize($firm, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Put("/firm/{id}")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putFirmAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $firmsRepo = $em->getRepository(Firm::class);
        /** @var Firm $firm */
        $firm = $firmsRepo->find($id);
        $firm->setName($request->get('name'));
        $firm->setStatus(Firm::STATUS_ACTIVE);

        if ($request->get('categories')) {
            $categories = $em->getRepository(Category::class)->findBy(['id' => $request->get('categories')]);
            $firm->setCategories($categories);
        } else {
            $firm->setCategories([]);
        }

        $errors = $validator->validate($firm);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $em->persist($firm);
        $em->flush();

        $context = new Context();
        $context->addGroup('api_firm');
        $data = $this->serializerAdapter->serialize($firm, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Get("/firm")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function getFirmsAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();
        $firmsRepo = $em->getRepository(Firm::class);
        /** @var Firm[] $firms */
        $firms = $firmsRepo->findBy(['status' => Firm::STATUS_ACTIVE]);

        foreach ($firms as $firm) {
            $firm->removeDeletedSeries();
        }

        $context = new Context();
        $context->addGroup('api_firm');
        $data = $this->serializerAdapter->serialize(array_values($firms), 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Delete("/firm/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteFirmAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $firmsRepo = $em->getRepository(Firm::class);
        /** @var Firm $firm */
        $firm = $firmsRepo->find($id);

        $firm->deleteRecursively();
        $em->persist($firm);

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Post("/firm_series")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function postFirmSeriesAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userProvider->authUser($request);
        $em = $this->getDoctrine()->getManager();

        $firmSeries = new FirmSeries();
        $firmSeries->setName($request->get('name'));

        $firmSeries->setCreatedBy($user);
        $firmSeries->setStatus(FirmSeries::STATUS_ACTIVE);

        $firm = $em->getRepository(Firm::class)
            ->findOneBy(['id' => $request->get('firm')]);
        $firmSeries->setFirm($firm);

        if ($request->get('categories')) {
            $categories = $em->getRepository(Category::class)->findBy(['id' => $request->get('categories')]);
            $firmSeries->setCategories($categories);
        }

        $errors = $validator->validate($firmSeries);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $em->persist($firmSeries);
        $em->flush();

        $context = new Context();
        $context->addGroup('api_firm');
        $data = $this->serializerAdapter->serialize($firmSeries, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Put("/firm_series/{id}")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function putFirmSeriesAction(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $firmsSeriesRepo = $em->getRepository(FirmSeries::class);
        /** @var FirmSeries $firmSeries */
        $firmSeries = $firmsSeriesRepo->find($id);
        $firmSeries->setName($request->get('name'));
        $firmSeries->setStatus(Firm::STATUS_ACTIVE);

        if ($request->get('categories')) {
            $categories = $em->getRepository(Category::class)->findBy(['id' => $request->get('categories')]);
            $firmSeries->setCategories($categories);
        } else {
            $firmSeries->setCategories([]);
        }

        $errors = $validator->validate($firmSeries);

        if (\count($errors) > 0) {
            return $this->json(ValidationHelper::getErrorsArray($errors), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em->persist($firmSeries);
        $em->flush();

        $context = new Context();
        $context->addGroup('api_firm');
        $data = $this->serializerAdapter->serialize($firmSeries, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @FOSRest\Delete("/firm_series/{id}")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     */
    public function deleteFirmSeriesAction(Request $request): JsonResponse
    {
        $this->userProvider->authUser($request);
        $id = $request->get('id');

        if (!$id) {
            return $this->json(['id' => 'id.not_blank'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $firmSeriesRepo = $em->getRepository(FirmSeries::class);
        /** @var FirmSeries $firmSeries */
        $firmSeries = $firmSeriesRepo->find($id);

        if (count($firmSeries->getProducts()) > 0) {
            $metadata = [];
            foreach ($firmSeries->getProducts() as $product) {
                $metadata[] = $product->getName();
            }

            if (count($metadata) > 0) {
                return $this->json(['reason' => 'existed.product', 'metadata' => $metadata], Response::HTTP_BAD_REQUEST);
            }
        }

        $firmSeries->setStatus(Firm::STATUS_DELETED);
        $em->persist($firmSeries);

        $em->flush();

        return $this->json(null, Response::HTTP_CREATED);
    }
}